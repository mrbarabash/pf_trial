<?php

namespace App\Manager;

use App\Model\BoardingCardInterface;

class PathFinder
{
    /**
     * @param BoardingCardInterface[] $boardingCards
     *
     * @return BoardingCardInterface[]
     */
    public function sortBoardingCards(array $boardingCards): array
    {
        // TODO: Add validation for the case if we've got duplicated cards (field from and to are the same).

        $arrivals = $departures = [];
        $sortedBoardingCards = [];

        // Sort all boarding cards by arrivals/departures
        foreach ($boardingCards as $boardingCard) {
            $arrivals[$boardingCard->getVoyageTo()] = $boardingCard;
            $departures[$boardingCard->getVoyageFrom()] = $boardingCard;
        }

        // Find start and end places
        $startPlace = array_diff(array_keys($departures), array_keys($arrivals));
        $startPlace = reset($startPlace);
        $endPlace = array_diff(array_keys($arrivals), array_keys($departures));
        $endPlace = reset($endPlace);

        /** @var BoardingCardInterface $boardingCard */
        $boardingCard = $departures[$startPlace];
        $sortedBoardingCards[] = $boardingCard;

        do {
            $boardingCard = $departures[$boardingCard->getVoyageTo()];
            $sortedBoardingCards[] = $boardingCard;
        } while ($boardingCard->getVoyageTo() !== $endPlace);

        return $sortedBoardingCards;
    }
}
