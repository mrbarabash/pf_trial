<?php

namespace App\Model;

class BusBoardingCard extends AbstractBoardingCard
{
    public function __construct()
    {
        $this->setType(self::TYPE_BUS);
    }
}
