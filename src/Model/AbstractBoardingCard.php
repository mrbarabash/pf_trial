<?php

namespace App\Model;

abstract class AbstractBoardingCard implements BoardingCardInterface, \JsonSerializable
{
    const TYPE_BUS = 'bus';
    const TYPE_FLIGHT = 'flight';

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $voyageNo;

    /**
     * TODO No seat preferences
     *
     * @var string
     */
    private $seat;

    /**
     * @var string
     */
    private $voyageFrom;

    /**
     * @var string
     */
    private $voyageTo;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getAvailableTypes(): array
    {
        // TODO: Automate it using reflection.

        return [self::TYPE_BUS, self::TYPE_FLIGHT];
    }

    /**
     * @param string $type
     *
     * @return BoardingCardInterface
     */
    public function setType(string $type): BoardingCardInterface
    {
        $this->ensureCorrectType($type);

        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getVoyageNo(): string
    {
        return $this->voyageNo;
    }

    /**
     * @param string $voyageNo
     *
     * @return BoardingCardInterface
     */
    public function setVoyageNo(string $voyageNo): BoardingCardInterface
    {
        $this->voyageNo = $voyageNo;

        return $this;
    }

    /**
     * @return string
     */
    public function getSeat(): string
    {
        return $this->seat;
    }

    /**
     * @param string $seat
     *
     * @return BoardingCardInterface
     */
    public function setSeat(string $seat): BoardingCardInterface
    {
        $this->seat = $seat;

        return $this;
    }

    /**
     * @return string
     */
    public function getVoyageFrom(): string
    {
        return $this->voyageFrom;
    }

    /**
     * @param string $voyageFrom
     *
     * @return BoardingCardInterface
     */
    public function setVoyageFrom(string $voyageFrom): BoardingCardInterface
    {
        $this->voyageFrom = $voyageFrom;

        return $this;
    }

    /**
     * @return string
     */
    public function getVoyageTo(): string
    {
        return $this->voyageTo;
    }

    /**
     * @param string $voyageTo
     *
     * @return BoardingCardInterface
     */
    public function setVoyageTo(string $voyageTo): BoardingCardInterface
    {
        $this->voyageTo = $voyageTo;

        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->getJsonSerializationData();
    }

    /**
     * @return array
     */
    protected function getJsonSerializationData(): array
    {
        return [
            'type' => $this->getType(),
            'voyage_no' => $this->getVoyageNo(),
            'from' => $this->getVoyageFrom(),
            'to' => $this->getVoyageTo(),
            'seat' => $this->getSeat(),
        ];
    }

    /**
     * TODO Move to Contracts
     *
     * @param string $type
     *
     * @throws \InvalidArgumentException
     */
    private function ensureCorrectType(string $type)
    {
        if (!in_array($type, $this->getAvailableTypes())) {
            throw new \InvalidArgumentException(sprintf('%s type is not defined yet', $type));
        }
    }
}
