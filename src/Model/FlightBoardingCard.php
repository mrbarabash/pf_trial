<?php

namespace App\Model;

class FlightBoardingCard extends AbstractBoardingCard
{
    /**
     * @var string
     */
    private $gate;

    public function __construct()
    {
        $this->setType(self::FLI);
    }

    /**
     * @return string
     */
    public function getGate()
    {
        return $this->gate;
    }

    /**
     * @param string $gate
     *
     * @return self
     */
    public function setGate(string $gate): self
    {
        $this->gate = $gate;

        return $this;
    }

    /**
     * @return array
     */
    protected function getJsonSerializationData(): array
    {
        return array_merge(parent::getJsonSerializationData(), ['gate' => $this->getGate()]);
    }
}
