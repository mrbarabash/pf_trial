<?php

namespace App\Model;

interface BoardingCardInterface
{
    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @param string $type
     *
     * @return BoardingCardInterface
     */
    public function setType(string $type): BoardingCardInterface;

    /**
     * @return string
     */
    public function getVoyageNo(): string;

    /**
     * @param string $voyageNo
     *
     * @return BoardingCardInterface
     */
    public function setVoyageNo(string $voyageNo): BoardingCardInterface;

    /**
     * @return string
     */
    public function getSeat(): string;

    /**
     * @param string $seat
     *
     * @return BoardingCardInterface
     */
    public function setSeat(string $seat): BoardingCardInterface;

    /**
     * @return string
     */
    public function getVoyageFrom(): string;

    /**
     * @param string $voyageFrom
     *
     * @return BoardingCardInterface
     */
    public function setVoyageFrom(string $voyageFrom): BoardingCardInterface;

    /**
     * @return string
     */
    public function getVoyageTo(): string;

    /**
     * @param string $voyageTo
     *
     * @return BoardingCardInterface
     */
    public function setVoyageTo(string $voyageTo): BoardingCardInterface;
}
