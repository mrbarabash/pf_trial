<?php

namespace App\Serializer;

use App\Model\BoardingCardInterface;
use App\Model\FlightBoardingCard;

/**
 * Serialize and deserialize boarding card arrays
 */
class BoardingCardSerializer
{
    const MODEL_PREFIX = 'App\\Model\\';

    /**
     * @param string $json
     *
     * @return BoardingCardInterface[]
     */
    public function jsonToObjects(string $json): array
    {
        $objects = [];
        $objectsArray = json_decode($json, true);

        // TODO Better deserialization and validation
        if ($objectsArray) {
            foreach ($objectsArray as $objectArray) {
                $boardingCardClass = $this->getBoardingClass($objectArray['type']);
                $this->ensureClassExists($boardingCardClass);

                /** @var BoardingCardInterface $boardingCard */
                $boardingCard = new $boardingCardClass;
                $boardingCard
                    ->setVoyageNo($objectArray['voyage_no'])
                    ->setSeat($objectArray['seat'])
                    ->setVoyageFrom($objectArray['voyage_from'])
                    ->setVoyageTo($objectArray['voyage_to']);

                if ($boardingCard instanceof FlightBoardingCard) {
                    $boardingCard->setGate($objectArray['gate']);
                }

                $objects[] = $boardingCard;
            }
        }

        return $objects;
    }

    /**
     * @param array $objects
     *
     * @return string
     */
    public function objectsToJson(array $objects): string
    {
        return json_encode($objects, JSON_PRETTY_PRINT);
    }

    /**
     * @param string $type
     *
     * @return string FQCN
     */
    private function getBoardingClass(string $type): string
    {
        return self::MODEL_PREFIX.ucfirst($type).'BoardingCard';
    }

    /**
     * @param string $class
     *
     * @throws \LogicException
     */
    private function ensureClassExists(string $class)
    {
        if (!class_exists($class)) {
            throw new \LogicException(sprintf('Class "%s" doesn\'t exist', $class));
        }
    }
}
