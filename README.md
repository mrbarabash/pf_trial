# PropertyFinder Trial Case
PropertyFinder Trial Case is the implementation for the test case described [here](https://docs.google.com/document/d/1I2cJ7cH-P7SqLVdOpWY0OsZRIlngav2IKMzHJgL3Bp0).

## Code Requirements
- PHP 7.1+

## Project Structure
### Manager
Here is placed `PathFinder` and could be placed any other manager to store business logic to work with data models.

### Model
Here are all models you need to represent data.

### Serializer
Simple `BoardingCardSerializer` is stored here.

## Usage

Let's assume you've got unsorted boarding cards data in JSON format.

### 1. Deserialize data into objects

```php
use App\Serializer\BoardingCardSerializer;

$jsonData = '...';

$boardingCards = (new BoardingCardSerializer())->jsonToObjects($jsonData);
```

### 2. Sort your boarding cards using PathFinder manager

```php
use App\Manager\PathFinder;

$sortedBoardingCards = (new PathFinder())->sortBoardingCards($boardingCards);
```

## Testing
Run
```ssh
$ vendor/bin/phpunit tests
```

## Extending with other boarding card types
### 1. Implement class extending `AbstractBoardingCard`
```php

namespace App\Model;

class TaxiBoardingCard extends AbstractBoardingCard
{
    // ...
}

```

### 2. Add new fields and override method `getJsonSerializationData`
```php

namespace App\Model;

class TaxiBoardingCard extends AbstractBoardingCard
{
    /**
     * @var string
     */
    private $driverPhone;
    
    // ...
    
    /**
     * @return array
     */
    protected function getJsonSerializationData(): array
    {
        return array_merge(parent::getJsonSerializationData(), ['driver_phone' => $this->driverPhone]);
    }
}
```

### 3. Add case to `BoardingCardSerializer::jsonToObjects()`
```php

namespace App\Serializer;

use use App\Model\{FlightBoardingCard, TaxiBoardingCard};

class BoardingCardSerializer
{
    // ...
    public function jsonToObjects(string $json): array
    {
        // ...
        
        if ($boardingCard instanceof FlightBoardingCard) {
            $boardingCard->setGate($objectArray['gate']);
        }
        if ($boardingCard instanceof TaxiBoardingCard) {
            $boardingCard->setDriverPhone($objectArray['driver_phone']);
        }
    }
}
```

## TODO
1. Implement templating in order to have human-readable output for result trip.
2. Add data validation.
3. Add possibility to do not have seat.
4. Add serializer (JMSSerializer, etc).
