<?php

namespace App\Tests\Serializer;

use App\Serializer\BoardingCardSerializer;
use App\Tests\BoardingCardsTestData;
use PHPUnit\Framework\TestCase;

class BoardingCardSerializerTest extends TestCase
{
    public function testDeserialization()
    {
        $boardingCards = (new BoardingCardSerializer())
            ->jsonToObjects(json_encode(BoardingCardsTestData::getBoardingCardsArray(), JSON_PRETTY_PRINT));
        $boardingCardsSample = BoardingCardsTestData::getBoardingCardsObjects();

        static::assertEquals($boardingCardsSample, $boardingCards);
    }

    public function testSerialization()
    {
        $boardingCardsJson = (new BoardingCardSerializer())
            ->objectsToJson(BoardingCardsTestData::getBoardingCardsObjects());
        $boardingCardsSample = json_encode(BoardingCardsTestData::getBoardingCardsObjects(), JSON_PRETTY_PRINT);

        static::assertEquals($boardingCardsSample, $boardingCardsJson);
    }
}
