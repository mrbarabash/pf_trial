<?php

namespace App\Tests\Manager;

use App\Manager\PathFinder;
use App\Tests\BoardingCardsTestData;
use PHPUnit\Framework\TestCase;

class PathFinderTest extends TestCase
{
    public function testSortOrder()
    {
        $sortedBoardingCards = $shuffledBoardingCards = BoardingCardsTestData::getBoardingCardsObjects();
        shuffle($shuffledBoardingCards);

        static::assertNotEquals($sortedBoardingCards, $shuffledBoardingCards);
        static::assertEquals($sortedBoardingCards, (new PathFinder())->sortBoardingCards($shuffledBoardingCards));
    }
}
