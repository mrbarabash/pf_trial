<?php

namespace App\Tests;

use App\Model\BoardingCardInterface;
use App\Model\BusBoardingCard;
use App\Model\FlightBoardingCard;

class BoardingCardsTestData
{
    /**
     * @return array
     */
    public static function getBoardingCardsArray(): array
    {
        return [[
            'type' => 'bus',
            'voyage_no' => '11',
            'seat' => '14B',
            'voyage_from' => 'Berlin Train Station',
            'voyage_to' => 'Berlin Tegel Airport',
        ], [
            'type' => 'flight',
            'voyage_no' => 'OS226',
            'seat' => '3A',
            'gate' => 'C11',
            'voyage_from' => 'Berlin Tegel Airport',
            'voyage_to' => 'Prague Václav Havel Airport',
        ], [
            'type' => 'bus',
            'voyage_no' => '710',
            'seat' => '4',
            'voyage_from' => 'Prague Václav Havel Airport',
            'voyage_to' => 'Praha Main Railway Station',
        ], [
            'type' => 'bus',
            'voyage_no' => '154F',
            'seat' => '89A',
            'voyage_from' => 'Praha Main Railway Station',
            'voyage_to' => 'Vienna Central Station',
        ], [
            'type' => 'bus',
            'voyage_no' => '505',
            'seat' => '22',
            'voyage_from' => 'Vienna Central Station',
            'voyage_to' => 'Vienna International Airport',
        ], [
            'type' => 'flight',
            'voyage_no' => 'VY2020',
            'seat' => '28F',
            'gate' => '104',
            'voyage_from' => 'Vienna International Airport',
            'voyage_to' => 'Milan Malpensa Airport',
        ]];
    }

    /**
     * @return BoardingCardInterface[]
     */
    public static function getBoardingCardsObjects(): array
    {
        return [
            (new BusBoardingCard())
                ->setVoyageNo('11')
                ->setSeat('14B')
                ->setVoyageFrom('Berlin Train Station')
                ->setVoyageTo('Berlin Tegel Airport'),
            (new FlightBoardingCard())
                ->setVoyageNo('OS226')
                ->setSeat('3A')
                ->setVoyageFrom('Berlin Tegel Airport')
                ->setVoyageTo('Prague Václav Havel Airport')
                ->setGate('C11'),
            (new BusBoardingCard())
                ->setVoyageNo('710')
                ->setSeat('4')
                ->setVoyageFrom('Prague Václav Havel Airport')
                ->setVoyageTo('Praha Main Railway Station'),
            (new BusBoardingCard())
                ->setVoyageNo('154F')
                ->setSeat('89A')
                ->setVoyageFrom('Praha Main Railway Station')
                ->setVoyageTo('Vienna Central Station'),
            (new BusBoardingCard())
                ->setVoyageNo('505')
                ->setSeat('22')
                ->setVoyageFrom('Vienna Central Station')
                ->setVoyageTo('Vienna International Airport'),
            (new FlightBoardingCard())
                ->setVoyageNo('VY2020')
                ->setSeat('28F')
                ->setVoyageFrom('Vienna International Airport')
                ->setVoyageTo('Milan Malpensa Airport')
                ->setGate('104'),
        ];
    }
}
